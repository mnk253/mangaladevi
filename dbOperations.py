import json
import os
import sys
import csv
from datetime import datetime
import pymysql
from db import get_connection


def create_db():

    conn = get_connection()
    cur = conn.cursor()
    query = "CREATE TABLE stock( ItemCode varchar(50) NOT NULL, ItemName varchar(50), ItemSize varchar(50), ItemGSM varchar(50), ItemBF varchar(50), Reels varchar(50), Quantity varchar(50), LastUpdated varchar(50))"
    cur.execute(query)
    conn.commit()
    conn.close()

def create(data):

    print(type(data))
    conn = get_connection()
    if not conn:
        return
    try:
        cur = conn.cursor()
        cur.execute(
            "INSERT INTO stock (ItemCode, ItemName, ItemSize, ItemGSM, ItemBF, Reels, Quantity, LastUpdated) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)",
            (
                data["ItemCode"],
                data["ItemName"],
                data["ItemSize"],
                data["ItemGSM"],
                data["ItemBF"],
                data["Reels"],
                data["Quantity"],
                data["LastUpdated"],
            ),
        )
        conn.commit()
    except Exception as ex:

        raise (ex)
    finally:
        conn.close()


def filter_products(products, data):

    if not data:
        return products

    keys = list(data.keys())
    products = [
        {key: val for key, val in dict(item).items()}
        for item in products
    ]

    filtered_products = []
    query = data.get("ItemName", "")
    filter_param = {key: val for key, val in data.items() if val}
    print(filter_param)
    filtered_products = []
    if query:

        
        for item in products:
            cond_dict = []
            for key in filter_param:
                if key == "ItemName":
                    value = data.get(key).lower() in item.get(key).lower()
                else:
                    value = data.get(key) == item.get(key)
                cond_dict.append(value)
                print(cond_dict)
            if len(set(cond_dict)) == 1 and list(set(cond_dict))[0] == True:
                filtered_products.append(item)

    else:

        for item in products:
            for key in set(filter_param) - {"ItemName"}:
                flag = 0
                if data.get(key) == item.get(key):
                    flag = 1
            if flag == 1:
                filtered_products.append(item)

    return filtered_products


def getAll(data):

    print(data)
    query = "select * from stock"
    conn = get_connection()
    print("Connected", conn)
    if not conn:
        return
    try:

        cur = conn.cursor(pymysql.cursors.DictCursor)
        cur.execute(query)
        products = cur.fetchall()
        print(products)
    except Exception as ex:
        print(ex)
        raise ex
    finally:
        conn.close()
        f_products = filter_products(products, data)
        print("db", f_products)
        return f_products


def updateItem(code, updated):

    conn = get_connection()
    if not conn:
        return

    constraints = ", ".join(
        [f"{key} = {str(value)}" for key, value in updated.items() if key != "ItemCode"]
    )
    query = f"Update stock set {constraints} where ItemCode={code}"
    print(query)
    try:
        cur = conn.cursor(pymysql.cursors.DictCursor)
        cur.execute(query)
        conn.commit()
    except Exception as ex:
        raise (ex)

    finally:
        conn.close()


def generate(frmdate, todate):

    products = getAll({})
    returnData = []
    print(frmdate, todate)
    if not frmdate and not todate:
        return products

    if frmdate and not todate:

        for item in products:
            updateDate = item["LastUpdated"]
            print(updateDate)
            if datetime.strptime(updateDate, "%Y%m%d") >= datetime.strptime(
                frmdate, "%Y-%m-%d"
            ):
                returnData.append(item)
        return returnData

    if todate and not frmdate:

        for item in products:
            updateDate = item["LastUpdated"]
            print(updateDate)
            if datetime.strptime(updateDate, "%Y%m%d") <= datetime.strptime(
                todate, "%Y-%m-%d"
            ):
                returnData.append(item)
        return returnData

    for item in products:
        updateDate = item["LastUpdated"]
        print(updateDate)
        if datetime.strptime(updateDate, "%Y%m%d") <= datetime.strptime(
            todate, "%Y-%m-%d"
        ) and datetime.strptime(updateDate, "%Y%m%d") >= datetime.strptime(
            frmdate, "%Y-%m-%d"
        ):
            returnData.append(item)

    return returnData


def deleteItem(data):

    conn = get_connection()
    if not conn:
        return

    if data:
        query = f"delete from stock where ItemCode={data.get('ItemCode')}"
    else:
        query = "delete from stock"
    try:
        print(query)
        cur = conn.cursor()
        cur.execute(query)
        conn.commit()
    except Exception as ex:
        raise (ex)
    finally:
        conn.close()
