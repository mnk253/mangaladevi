CREATE DATABASE mangladb;
use mangladb;



CREATE TABLE `stock` (
	`ItemCode` varchar(50) NOT NULL,
	`ItemName` varchar(50),
	`ItemSize` varchar(50),
	`ItemGSM` varchar(50),
	`ItemBF` varchar(50),
	`Reels` varchar(50),
	`Quantity` varchar(50),
	`LastUpdated` varchar(50)
)
