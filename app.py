from flask import render_template, Flask, request
import json
from datetime import datetime
import time
import os
from flask import send_from_directory     
from dbOperations import *


app = Flask(__name__)

@app.route('/favicon.ico') 
def favicon(): 
    return send_from_directory(os.path.join(app.root_path, 'static'), 'favicon.ico', mimetype='image/vnd.microsoft.icon')


@app.route('/create_db')
def create_db():

    create_db()
    return render_template("index.html")


@app.route("/home")
def home():
    return render_template("index.html")


@app.route("/load_add_stock")
def loadAddStock():
    return render_template("stockAdd.html")


@app.route("/add_stock")
def add_stock():

    item = request.args
    dateval = datetime.now().strftime("%Y%m%d")
    itemStore = {
        "ItemCode": str(time.time()).replace(".", ""),
        "ItemName": item.get("txtname"),
        "ItemSize": item.get("txtsize"),
        "ItemGSM": item.get("txtgsm"),
        "ItemBF": item.get("txtbf"),
        "Reels": item.get("txtreels", "0"),
        "Quantity": item.get("txtquantity", "0"),
        "LastUpdated": dateval,
    }
    print(itemStore)
    create(itemStore)
    return render_template("stockAdd.html")


@app.route("/load_search")
def load_search():

    return render_template("searchUpdate.html")


@app.route("/add_consume_load")
def load_add_consume():

    return render_template("AddConsume.html")


@app.route("/search_add_consume")
def search_add_consume():

    item = dict(request.args)
    searchParams = {key: value for key, value in item.items() if value}
    searchStore = {
        "ItemName": item.get("txtname", None),
        "ItemSize": item.get("txtsize", None),
        "ItemGSM": item.get("txtgsm", None),
        "ItemBF": item.get("txtbf", None),
    }
    if not searchParams:
        products = getAll({})
    else:
        products = getAll(searchStore)

    return render_template("AddConsume.html", products=products)


@app.route("/<item_code>", methods=["POST", "GET"])
def add_consume_product_load(item_code):

    print(item_code)
    searchStore = {"ItemCode": item_code}
    products = getAll(searchStore)
    print(products)
    return render_template("AddConsumeProduct.html", products=products)


@app.route("/search_operation")
def search():

    item = dict(request.args)
    searchParams = {key: value for key, value in item.items() if value}
    searchStore = {
        "ItemName": item.get("txtname", None),
        "ItemSize": item.get("txtsize", None),
        "ItemGSM": item.get("txtgsm", None),
        "ItemBF": item.get("txtbf", None),
    }
    if not searchParams:
        products = getAll({})
    else:
        products = getAll(searchStore)

    return render_template("searchUpdate.html", products=products)


@app.route("/update_data", methods=["POST", "GET"])
def update_load():

    code = request.args.get("keyword")
    searchStore = {"ItemCode": code}
    products = getAll(searchStore)
    print(products)
    return render_template("updateProduct.html", products=products)


@app.route("/add_consume_update", methods=["POST", "GET"])
def add_value():

    item = dict(request.form)
    print(item)
    products = getAll({"ItemCode": item.get("txtcode")})
    qty = int(item.get("txtquantity", "0")) if item.get("txtquantity", "0") else "0"
    reels = int(item.get("txtreels", "0")) if item.get("txtreels", "0") else "0"
    print(reels, qty)

    if item["action"] == "CONSUME":
        oldqty = int(products[0]["Quantity"])
        oldreels = int(products[0]["Reels"])
        # print(oldreels, oldqty)
        newqty = str(oldqty - qty)
        newreels = str(oldreels - reels)
        updateItem(item.get("txtcode"), {"Quantity": newqty, "Reels": newreels})

    if item["action"] == "ADD":

        oldqty = int(products[0]["Quantity"])
        oldreels = int(products[0]["Reels"])
        # print(oldreels, oldqty)
        newqty = str(oldqty + qty)
        newreels = str(oldreels + reels)
        updateItem(item.get("txtcode"), {"Quantity": newqty, "Reels": newreels})

    if item["action"] == "DELETE_DATA":

        deleteStore = {"ItemCode": item.get("txtcode")}
        deleteItem(deleteStore)        

    
    return render_template("index.html")


@app.route("/update_product")
def update_product():

    print("Haiiiii")
    item = dict(request.args)
    dateval = datetime.now().strftime("%Y%m%d")
    print(dateval)
    itemStore = {
        "ItemCode": item.get("txtcode"),
        "ItemName": item.get("txtname"),
        "ItemSize": item.get("txtsize"),
        "ItemGSM": item.get("txtgsm"),
        "ItemBF": item.get("txtbf"),
        "Reels": item.get("txtreels"),
        "Quantity": item.get("txtquantity"),
        "LastUpdated": dateval,
    }
    updateItem(item.get("txtcode"), itemStore)
    return render_template("index.html")

# Report Operations

@app.route("/load_report")
def load_report():

    essentials = {}
    return render_template("Report.html", essentials=essentials)


@app.route("/report_generate")
def generateReport():

    item = dict(request.args)
    fromdate = item.get("txtfromdate")
    todate = item.get("txttodate")
    products = generate(fromdate, todate)
    tempproducts = products
    reels_sum = sum([int(i["Reels"]) for i in tempproducts])
    qty_sum = sum([int(i["Quantity"]) for i in tempproducts])
    print(reels_sum, qty_sum)
    currdate = todate if todate else datetime.now().strftime("%Y-%m-%d")
    essentials = {"todate": currdate, "reels_sum": reels_sum, "qty_sum": qty_sum}
    return render_template("Report.html", products=products, essentials=essentials)



# Delete Operations


@app.route("/delete")
def delete_product():
    
    item = dict(request.args)
    deleteStore = {"ItemCode": item.get("ItemCode")}
    deleteItem(deleteStore)
    return render_template("index.html")



@app.route("/delete_all")
def delete_record():

    deleteItem({})
    return render_template("index.html")


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
